#!/usr/bin/env fish

argparse --name install "global" -- $argv

if ! test -z $_flag_global
  set fish_directory /etc/fish
else
  set fish_directory $HOME/.config/fish
end

set fish_config $fish_directory/config.fish

set sugar_modules_directory $fish_directory/modules.d
set sugar_modules_config_directory $fish_directory/modules.conf.d

if ! test -d $fish_directory/conf.d
  mkdir -p $fish_directory/conf.d
end

cp ./sugar.fish $fish_directory/conf.d/sugar.fish

if ! test -d $sugar_modules_directory
  mkdir -p $sugar_modules_directory
end

if ! test -d $sugar_modules_config_directory
  mkdir -p $sugar_modules_config_directory
end

if test -f $fish_config
  mv $fish_config $fish_directory/config.fish.old-(date +%s)
end

echo "\
# Remove fish greeting.
set --universal fish_greeting

# Set fish shell directory.
set fish_directory $fish_directory

# Set the sugar modules directory.
set sugar_modules_directory $sugar_modules_directory

# Set the sugar config directory.
set sugar_modules_config_directory $sugar_modules_config_directory

# Set modules to load.
set modules \\
  sugarush/fish:developer \\
  sugarush/fish:completions \\
  sugarush/fish:prompt \\
  sugarush/fish:cd \\
  sugarush/fish:grc \\
  sugarush/fish:git \\
  sugarush/fish:ssh \\
  sugarush/fish:tmux

# If running macOS, append `homebrew` module.
if test (uname) = \"Darwin\"
  set --append modules sugarush/fish:homebrew
end

# Load modules.
for module in \$modules
  sugar-load \$module
end

# Trigger on-load hooks.
sugar-on-load \
" > $fish_config

if test -z $_flag_global
  exec fish
end
