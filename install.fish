#!/usr/bin/env fish

set fish_config $HOME/.config/fish/config.fish

if ! test -d $HOME/.config/fish/sugarush/modules
  mkdir -p $HOME/.config/fish/sugarush/modules
end

if ! test -d $HOME/.config/fish/sugarush/config
  mkdir -p $HOME/.config/fish/sugarush/config
end

if ! test -f $fish_config
  echo '
# Set sugar install directory.
set sugar_install_directory $HOME/.config/fish/sugarush

# Source sugar functions.
source $sugar_install_directory/sugar.fish

# Set modules to load.
set modules \
  sugarush/fish:developer \
  sugarush/fish:prompt \
  sugarush/fish:cd \
  sugarush/fish:git \
  sugarush/fish:ssh \
  sugarush/fish:tmux

# Load modules.
for module in $modules
  sugar-load $module
end

# Trigger on-load hooks.
sugar-on-load

# Source local fish config.
for file in $HOME/.config/fish/conf.d/**
  source $file
end

# Remove fish greeting.
set --universal fish_greeting
' > $fish_config

  exec fish
end
