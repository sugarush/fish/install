set sugar_gitlab_url 'https://gitlab.com'
set sugar_on_load

function @sugar-load -d 'Run a function when the shell is loaded.'
  set --append sugar_on_load $argv[1]
end

function sugar-on-load -d 'Run functions in the sugar_on_load array.'
  for item in $sugar_on_load
    set item_split (string split ' ' $item)
    $item_split[1] $item_split[2..-1]
  end
end

function sugar-install -d 'Install a module.'
  set -l module_info (string split ':' $argv[1])
  set -l module_repository $module_info[1]
  set -l module_name $module_info[2]
  set -l module_path $sugar_modules_directory/$module_repository/$module_name

  sugar-message title "Installing $argv[1] from GitLab."

  git clone $sugar_gitlab_url/$module_repository/$module_name.git $module_path

  if ! test $status -eq 0
    sugar-message error "Install failed."
  end
end

function sugar-load -d 'Load a module.'
  set -l module_info (string split ':' $argv[1])
  set -l module_repository $module_info[1]
  set -l module_name $module_info[2]
  set -l module_path $sugar_modules_directory/$module_repository/$module_name

  if ! test -d $module_path
    sugar-install $argv
  end

  source $module_path/*.fish $module_path
end

function sugar-update -d "Update a module."
  if test -z $argv[1]
    sugar-message error "You must provide a module."
    return 1
  end

  set -l module_info (string split ':' $argv[1])
  set -l module_repository $module_info[1]
  set -l module_name $module_info[2]
  set -l module_path $sugar_modules_directory/$module_repository/$module_name

  if not test -d $module_path
    sugar-message error "Module $argv[1] not found."
    return 1
  end

  sugar-message title "$argv[1]"
  git -C $module_path pull
end

function sugar-update-all -d "Update all modules."
  for module in $modules
    sugar-update $module
  end
end

function sugar-upgrade -d "Upgrade sugar."
  sugar-message title "Upgrading..."
  curl "https://gitlab.com/sugarush/fish/install/-/raw/master/sugar.fish?ref_type=heads" --output "$fish_directory/conf.d/sugar.fish" --no-progress-meter
  sugar-message info "Upgrade complete."
  sugar-message bold "Reloading..."
  sugar-reload
end

function sugar-message -d 'Print a formatted message.'
  set -l options $argv[1]
  set -l message $argv[2]

  set -l options_split (string split ':' $options)
  set -l options_type $options_split[1]
  set -l options_operation $options_split[2]

  if test -n "$options_operation"
    switch $options_operation
    case reprint
      printf '\e[2K\r'
    case '*'
      sugar-message 'error' "invalid message option: $options_operation"
      return
    end
  end

  switch $options_type
  case title
    set_color -o green
    echo -n '==> '
  case bold
    set_color -o magenta
    echo -n '--> '
  case error
    set_color -o red
    echo -n '--> '
  case info
    set_color normal
    echo -n '--> '
  case '*'
    sugar-message 'error' "invalid message type: $options_type"
    return
  end

  if test -n "$options_operation"
    switch $options_operation
    case reprint
      echo -n $message
      set_color normal
    end
  else
    echo $message
    set_color normal
  end
end

function sugar-reload -d 'Reload Fish.'
  exec fish
end

function reload -d 'Reload Fish.'
  exec fish
end
